from unittest import TestCase
from ea import Evaluation


class TestEvaluation_function(TestCase):
    def test_result(self):
        e = Evaluation()
        ar = [1, -1, 1, -1]
        self.assertEqual(e.evaluation_function(ar), 14, 'Incorrect result')
        ar2 = [1, 1, -1, 1, -1]
        self.assertEqual(e.evaluation_function(ar2), 6, 'Incorrect result')
        ar3 = [-1, -1, 1, 1, -1, -1, 1, -1, 1]
        self.assertEqual(e.evaluation_function(ar3), 16, 'Incorrect result')
