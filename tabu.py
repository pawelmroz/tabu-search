import numpy as np
from copy import copy
import matplotlib.pyplot as plt
import time

class Tabu():
	def __init__(self, n = 4):
		self.size_of_array = n
		self.max_iterations = range(n*100)
		array = np.random.choice((-1, 1), self.size_of_array)
		self.best = array
		self.tabu_list = np.zeros(n)
		self.max_tabu_length = int(2*n/5)
		self.wyniki = []
		self.timeslot = []

		# Table of products
		self.tau = []
		for i in range(n-1):
			temp = []
			for j in range(n-i-1):
				temp.append(self.best[j]*self.best[j+i+1])
			self.tau.append(temp)

		# table of correlations (sum of products form one row)
		self.corr = [sum(vec) for vec in self.tau]

		print(self.best)



	def flip(self, k):
		"""
		Function return evaluation of array with flipped value on position k
		"""
		score = 0
		for i in range(self.size_of_array - 1):
			v = self.corr[i]
			if i < self.size_of_array - 1 - k:
				v = v - 2*self.tau[i][k]
			if i < k:
				v = v - 2*self.tau[i][k-i-1]

			score = score + v ** 2
		return score


	def update_tables(self, k):
		for i in range(self.size_of_array - 1):
			if i < self.size_of_array - 1 - k:
				self.tau[i][k] = -1 * self.tau[i][k]
			if i < k:
				self.tau[i][k - i - 1] = -1 * self.tau[i][k-i-1]
		self.corr = [sum(vec) for vec in self.tau]


	def tabu_search(self):
		"""
		Performs algorithm
		"""
		self.best_ever = copy(self.best)  # initilize best ever and best ever score
		self.best_ever_score = sum([el ** 2 for el in self.corr])
		best_score = sum([el ** 2 for el in self.corr])  # current best score
		timeslot = 0.01
		self.wyniki.append(self.best_ever_score)
		self.timeslot.append(timeslot)
		start = time.time()
		for it in self.max_iterations:
		    if time.time() - start > timeslot:
				timeslot = timeslot + 0.01
				self.wyniki.append(self.best_ever_score)
				self.timeslot.append(timeslot)
			# print result of some iterations
			results = self.parse_file()
			if best_score == int(results[self.size_of_array]):
				break
			if it % 100 == 0:
				print(str(it) + ":")
				print("current:", best_score)
				print("best ever:", self.best_ever_score)

			# initialize best neighbour
			best_neighbour = copy(self.best)
			bn_score = 10000000
			best_id = -1

			for i in range(len(self.best)):
				# create new solution with filpping one value
				new_solution = copy(self.best)
				new_solution[i] = -1 * new_solution[i]
				new_score = self.flip(i)

				# if operation is not on tabu list or
				if it >= self.tabu_list[i] or new_score < self.best_ever_score:
					if new_score < bn_score:  # if new score is better than current best neighbour score this is our best neighbour
						bn_score = new_score
						best_neighbour = copy(new_solution)
						best_id = i

			# make a move and update tables
			if best_id >= 0:
				self.best = copy(best_neighbour)
				self.tabu_list[best_id] = it + self.max_tabu_length
				self.update_tables(best_id)
			best_score = sum([el ** 2 for el in self.corr])

			# if our new solution is better than best ever we update info about best ever
			if best_score < self.best_ever_score:
				self.best_ever = copy(best_neighbour)
				self.best_ever_score = sum([el ** 2 for el in self.corr])
		return self.best_ever
		
	def parse_file(self):
		d = {}
		with open("best_results.txt") as f:
			for line in f:
				(key, val) = line.split()
				d[int(key)] = val
		return d


def evaluation_function(s):
	energy = 0
	size = len(s)
	for i in range(1, size):
		tmp = 0
		for j in range(0, size - i):
			tmp = tmp + s[j] * s[j + i]
		energy = energy + tmp ** 2
	return energy


tabu = Tabu(29)
result = tabu.tabu_search()
print(result)
print(evaluation_function(result))
plt.plot(tabu.timeslot, tabu.wyniki)
plt.xlabel("Time [s]")
plt.ylabel("Best result")
plt.show()