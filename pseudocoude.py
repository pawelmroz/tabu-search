def tabu_search():
	best_solution = s0
	best_option = s0
	tabu_list = []
	tabu_list.append(s0)
	while !should_stop()
		neighbours = get_neighbours(best_solution)
		for neighbour in neighbours
			if not neighbour in tabu_list and fitness(neighbour) < fitness(best_option)
				best_option = neighbour
		if fitness(best_option) < fitness(best_solution)
			best_solution = best_option
		tabu_list.append(best_option)
		if len(tabu_list) > tabu_limit
			tabu_list.pop(0)
	return best_solution