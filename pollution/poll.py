import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.cluster import KMeans
import numpy as np
import matplotlib.pyplot as plt
import requests

# function for clustering and visualization
def cluster(data, n, init):
    cmap = {0: 'g', 1: 'y', 2: 'r', 3: 'b', 4: 'k'}
    km = KMeans(n, init=init)
    clst = km.fit_predict(data)
    # fig = plt.figure()
    plt.figure(3)
    plt.xlabel('PM10')
    plt.ylabel('PM2.5')  
    plt.title('Clusters for pollution data')
    plt.scatter(data["PM10"], data["PM25"], c=[cmap[l] for l in clst])
    return clst

# read data from file
weather = pd.read_csv("weather_wind.csv", header=0, sep=";")

# outliers handling and visulaisation
plt.figure(1)
plt.plot(weather['PM10'], weather['PM25'], '.', label='Pollution')
x = np.arange(1, 450)
y = 0.6*x - 20
z = 0.8*x + 15
plt.plot(x, y, 'r', label='Inliers limits')
plt.plot(x, z, 'r')
plt.xlabel("PM10")
plt.ylabel("PM2.5")
plt.title('Pollution data with outliers')
plt.legend()

outliers = []
for i in range(len(weather)):
    try:
        if int(weather['PM25'][i]) < (0.6 * int(weather['PM10'][i]) - 20) or int(weather['PM25'][i]) > (0.8 * int(weather['PM10'][i]) + 15):
            outliers.append(i)
    except KeyError:
        continue

weather = weather.drop(weather.index[outliers])

plt.figure(2)
plt.plot(weather['PM10'], weather['PM25'], '.', label='Pollution')
x = np.arange(1, 450)
y = 0.6*x - 20
z = 0.8*x + 15
plt.plot(x, y, 'r', label='Inliers limits')
plt.plot(x, z, 'r')
plt.xlabel("PM10")
plt.ylabel("PM2.5")
plt.title('Pollution data without outliers')
plt.legend()


# removing rows with missing values
weather = weather[weather["TEMPERATURE"].notnull()]
weather = weather[weather["PRESSURE"].notnull()]
weather = weather[weather["HUMIDITY"].notnull()]
weather = weather[weather["WIND_DIRECTION"].notnull()]
weather = weather[weather["WIND_SPEED"].notnull()]


# clustaring
init3 = np.array([[0, 0], [100, 100], [200, 200]])
weather["message"] = cluster(weather[["PM10", "PM25"]], 3, init3)
weather = weather.drop(["PM10", "PM25"], axis=1)
print(weather.head())

# prepare data structures for machine learning part
train, test = train_test_split(weather, test_size=0.2)

clf = RandomForestClassifier(n_estimators=100, max_depth=5)

# train and predict model
clf.fit(train.iloc[:, 1:6],train.iloc[:, 6])

pred = clf.predict(test.iloc[:, 1:6])
cnt = 0
for a, b in zip(pred,test.iloc[:, 6]):
    # print("pred: ", a," - real: ", b)
    if a == b:
        cnt += 1
print("acc: ", cnt/len(pred))


#####
# Download wather prediction and prepare data for visualization
#####

def get_data(location):
    apikey = 'f5c41c88f7c9c02fcd1ab064d1274920'
    if location == 1: #Mydlniki
        url = 'http://api.openweathermap.org/data/2.5/forecast?q=Mydlniki,PL&APPID={}'.format(apikey)
    if location == 2: #Srodmiescie
        url = 'http://api.openweathermap.org/data/2.5/forecast?lat=50.07&lon=19.94&APPID={}'.format(apikey)
    if location == 3: #Podgorze
        url = 'http://api.openweathermap.org/data/2.5/forecast?lat=50.01&lon=19.96&APPID={}'.format(apikey)
    if location == 4: #Debniki
        url = 'http://api.openweathermap.org/data/2.5/forecast?lat=50.01&lon=19.87&APPID={}'.format(apikey)
    if location == 5: #Dabie
        url = 'http://api.openweathermap.org/data/2.5/forecast?lat=50.06&lon=19.98&APPID={}'.format(apikey)
    response = requests.get(url, stream=True).json()
    return response


def response_to_dataframe(response):
    list_of_days = [7, 15, 23, 31, 39]
    list = []
    for el in list_of_days:
        temp = round(response['list'][el]['main']['temp'] - 273, 2)
        pressure = response['list'][el]['main']['pressure']
        humidity = response['list'][el]['main']['humidity']
        wind_dir = response['list'][el]['wind']['deg']
        wind_speed = response['list'][el]['wind']['speed']
        sub_list = [temp, pressure, humidity, wind_dir, wind_speed]
        list.append(sub_list)
    columns = ['TEMPERATURE', 'PRESSURE', 'HUMIDITY', 'WIND_DIRECTION', 'WIND_SPEED']
    data = pd.DataFrame(list, columns=columns)
    return data


data = get_data(2)
df = response_to_dataframe(data)
print(df)
predictions = clf.predict(df)
print(predictions)
plt.show()
