import matplotlib.pyplot as plt
best_score = []
#iteration = []
#time = []

#logs = ["log_51_51.txt", "log_51_102.txt", "log_51_153.txt", "log_51_255.txt", "log_51_357.txt", "log_51_510.txt", "log_51_765.txt", "log_51_1020.txt", "log_51_2550.txt", "log_51_5100.txt"]
#logs = ["log_67_67.txt", "log_67_134.txt", "log_67_201.txt", "log_67_335.txt", "log_67_469.txt", "log_67_670.txt", "log_67_1005.txt", "log_67_1340.txt", "log_67_3350.txt", "log_67_6700.txt"]
logs = ["log_83_83.txt", "log_83_166.txt", "log_83_249.txt", "log_83_415.txt", "log_83_581.txt", "log_83_830.txt", "log_83_1245.txt", "log_83_1660.txt", "log_83_4150.txt", "log_83_8300.txt"]
iterations = [1, 2, 3, 5, 7, 10, 15, 20, 50, 100]
minimal = []
average = []
for log in logs:
    with open(log) as file:
        for line in file:
            best_score.append(int(line.split()[6]))
            #iteration.append(int(line.split()[8]))
            #time.append(float(line.split()[10]))
    minimal.append(min(best_score))
    average.append(sum(best_score)/len(best_score))
    print("Minimal value: {}".format(min(best_score)))
    print("Average value: {}".format(sum(best_score)/len(best_score)))

data = dict(zip(iterations, minimal))
data2 = dict(zip(iterations, average))
x, y = zip(*data.items())
a, b = zip(*data2.items())
plt.plot(x, y, linestyle='--', marker='o', label='Minimal result')
plt.plot(a, b, linestyle='--', marker='o', label='Averege result')
plt.xscale("log")
plt.xlabel("Iterations")
plt.ylabel("Result")
plt.legend()
plt.title("Minimal and average value for n = 83")
plt.show()


"""
data = dict(zip(iteration, best_score))
data2 = dict(sorted(data.items()))
data3 = dict(zip(time, best_score))
data4 = dict(sorted(data3.items()))
print(data3)
print(data4)
x, y = zip(*data2.items())
a, b = zip(*data4.items())
plt.plot(a, b)
plt.xlabel("Time")
plt.ylabel("Best result")
plt.show()
"""