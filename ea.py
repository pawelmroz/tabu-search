
class Evaluation:
    def evaluation_function(self, s):
        energy = 0
        size = len(s)
        for i in range(1, size):
            tmp = 0
            for j in range(0, size-i):
                tmp = tmp + s[j] * s[j + i]
            energy = energy + tmp**2
        return energy

